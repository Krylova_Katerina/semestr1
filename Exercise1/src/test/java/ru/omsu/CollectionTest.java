package ru.omsu;

import org.junit.Test;
import ru.omsu.institute.Institute;
import ru.omsu.institute.InstituteException;
import ru.omsu.trainee.Trainee;
import ru.omsu.trainee.TraineeException;

import java.util.*;

import static java.lang.System.currentTimeMillis;
import static java.lang.System.nanoTime;
import static junit.framework.TestCase.*;
import static org.junit.Assert.assertEquals;
import static ru.omsu.institute.Institute.printAllTraineeFromInstituteMap;
import static ru.omsu.institute.Institute.printTraineeFromInstituteMap;
import static ru.omsu.trainee.Trainee.printTraineeSet;

public class CollectionTest {
    @Test
    public void arrayListTest() {
        List<Trainee> trainees = new ArrayList<>();
        try {
            trainees.add(new Trainee("Vasya", "Popov", 5));
            trainees.add(new Trainee("Petr", "Sidorov", 3));
            trainees.add(new Trainee("Vasya", "Pupkin", 2));
            trainees.add(new Trainee("Roman", "Elina", 4));

            List<Trainee> reverseResult = new ArrayList<>();
            reverseResult.add(new Trainee("Roman", "Elina", 4));
            reverseResult.add(new Trainee("Vasya", "Pupkin", 2));
            reverseResult.add(new Trainee("Petr", "Sidorov", 3));
            reverseResult.add(new Trainee("Vasya", "Popov", 5));
            Collections.reverse(trainees);
            assertEquals(trainees, reverseResult);


            List<Trainee> resultRotate = new ArrayList<>();
            resultRotate.add(new Trainee("Petr", "Sidorov", 3));
            resultRotate.add(new Trainee("Vasya", "Popov", 5));
            resultRotate.add(new Trainee("Roman", "Elina", 4));
            resultRotate.add(new Trainee("Vasya", "Pupkin", 2));
            Collections.rotate(trainees, 2);
            assertEquals(trainees, resultRotate);

            Collections.shuffle(trainees);

            Trainee max = Collections.max(trainees, new Comparator<Trainee>() {
                @Override
                public int compare(Trainee o1, Trainee o2) {
                    return o1.getRating() - o2.getRating();
                }
            });
            assertEquals(max, new Trainee("Vasya", "Popov", 5));


            Collections.sort(trainees, new Comparator<Trainee>() {
                @Override
                public int compare(Trainee o1, Trainee o2) {
                    return o1.getRating() - o2.getRating();
                }
            });
            List<Trainee> resultSortByRating = new ArrayList<>();
            resultSortByRating.add(new Trainee("Vasya", "Pupkin", 2));
            resultSortByRating.add(new Trainee("Petr", "Sidorov", 3));
            resultSortByRating.add(new Trainee("Roman", "Elina", 4));
            resultSortByRating.add(new Trainee("Vasya", "Popov", 5));
            assertEquals(trainees, resultSortByRating);


            Collections.sort(trainees);
            List<Trainee> resultSortByName = new ArrayList<>();
            resultSortByName.add(new Trainee("Petr", "Sidorov", 3));
            resultSortByName.add(new Trainee("Roman", "Elina", 4));
            resultSortByName.add(new Trainee("Vasya", "Pupkin", 2));
            resultSortByName.add(new Trainee("Vasya", "Popov", 5));
            assertEquals(trainees, resultSortByName);

            //  assertEquals(Collections.binarySearch(trainees, "Petr"), new trainee("Petr", "Sidorov", 3);
        } catch (TraineeException e) {
            fail("TraineeException");
        }
    }

    @Test
    public void timeListTest() { //10
        List<Integer> linkedList = Collections.synchronizedList(new LinkedList<>());
        List<Integer> arrayList = Collections.synchronizedList(new ArrayList<>());
        for (int i = 0; i < 99999; i++) {
            linkedList.add(i);
            arrayList.add(i);
        }
        long start = currentTimeMillis();
        int elem = linkedList.get((int) Math.random());
        long finishLindedList = currentTimeMillis();
        long timeLinkedList = finishLindedList - start;

        long start1 = currentTimeMillis();
        elem = arrayList.get((int) Math.random());
        long finishArrayList = currentTimeMillis();
        long timeArrauList = finishArrayList - start1;

        System.out.println("Time ArrayList= " + timeArrauList);
        System.out.println("Time LinkedList= " + timeLinkedList);
        assertEquals(timeArrauList, timeLinkedList);

    }

    @Test
    public void traineeQueueTest() { //11
        Queue<Trainee> traineeQueue = new PriorityQueue<>();
        try {
            traineeQueue.add(new Trainee("Vasya", "Popov", 4));
            traineeQueue.add(new Trainee("Petr", "Sidorov", 3));
            traineeQueue.add(new Trainee("Vasya", "Pupkin", 2));
            traineeQueue.add(new Trainee("Roman", "Elina", 4));

            traineeQueue.poll();
            traineeQueue.poll();
            traineeQueue.poll();
            traineeQueue.poll();
            assertEquals(0, traineeQueue.size());
        } catch (TraineeException e) {
            fail("TraineeExeption");
        }
    }

    @Test
    public void traineeHashSetTest() { //12
        Set<Trainee> traineeSet = Collections.synchronizedSet(new HashSet<>());
        try {
            traineeSet.add(new Trainee("Vasya", "Popov", 4));
            traineeSet.add(new Trainee("Petr", "Sidorov", 3));
            traineeSet.add(new Trainee("Vasya", "Pupkin", 2));
            traineeSet.add(new Trainee("Roman", "Elina", 4));

            assertTrue(traineeSet.contains(new Trainee("Petr", "Sidorov", 3)));
            printTraineeSet(traineeSet);

        } catch (TraineeException e) {
            fail("TraineeExeption");
        }
    }

    @Test
    public void traineeTreeSetTest() {//13
        Set<Trainee> traineeSet = Collections.synchronizedSet(new TreeSet<>());
        try {
            traineeSet.add(new Trainee("Nikolay", "Popov", 4));
            traineeSet.add(new Trainee("Petr", "Sidorov", 3));
            traineeSet.add(new Trainee("Vasya", "Pupkin", 2));
            traineeSet.add(new Trainee("Roman", "Elina", 4));

            assertTrue(traineeSet.contains(new Trainee("Petr", "Sidorov", 3)));
            printTraineeSet(traineeSet);

        } catch (TraineeException e) {
            fail("TraineeExeption");
        }
    }

    @Test
    public void instituteHashSetTest() {//16
        Map<Trainee, Institute> instituteHashMap = Collections.synchronizedMap(new HashMap<>());
        try {
            Institute institute1 = new Institute("OMSU", "Omsk");
            Institute institute2 = new Institute("MSU", "Moskow");
            Institute institute3 = new Institute("Hogwarts", "London");
            instituteHashMap.put(new Trainee("Nikolay", "Popov", 4), institute1);
            instituteHashMap.put(new Trainee("Petr", "Sidorov", 3), institute2);
            instituteHashMap.put(new Trainee("Vasya", "Pupkin", 2), institute3);
            instituteHashMap.put(new Trainee("Roman", "Elina", 4), institute1);

            assertEquals(instituteHashMap.get(new Trainee("Petr", "Sidorov", 3)), institute2);

            printAllTraineeFromInstituteMap(instituteHashMap);

        } catch (TraineeException e) {
            fail("TraineeExeption");
        } catch (InstituteException e) {
            fail("InstituteException");
        }
    }

    @Test
    public void timeListHashSetTest() {//15
        List<Integer> arrayList = Collections.synchronizedList(new ArrayList<>());
        for (int i = 0; i < 100000; i++) {
            arrayList.add(i);
        }
        long start = currentTimeMillis();
        for (Integer k : arrayList) {
            arrayList.contains(k);
        }
        long timeList = currentTimeMillis() - start;

        Set<Integer> hashSet = Collections.synchronizedSet(new HashSet<>());
        for (int i = 0; i < 100000; i++) {
            hashSet.add(i);
        }
        start = currentTimeMillis();
        for (Integer k : hashSet) {
            hashSet.contains(k);
        }
        long timeHashSet = currentTimeMillis() - start;

        Set<Integer> treeSet = Collections.synchronizedSet(new TreeSet<>());
        for (int i = 0; i < 100000; i++) {
            hashSet.add(i);
        }
        start = currentTimeMillis();
        for (Integer k : treeSet) {
            treeSet.contains(k);
        }
        long timeTreeSet = currentTimeMillis() - start;

        System.out.println("Time List= " + timeList);
        System.out.println("Time TreeSet= " + timeTreeSet);
        System.out.println("Time HashSet= " + timeHashSet);
        assertEquals(timeList, timeHashSet);
        assertEquals(timeList, timeTreeSet);
        assertEquals(timeHashSet, timeTreeSet);

    }

    @Test
    public void treeSetTraineeTest() {//14
        Set<Trainee> traineeSet = Collections.synchronizedSet(new TreeSet<>());
        try {
            traineeSet.add(new Trainee("Nikolay", "Popov", 4));
            traineeSet.add(new Trainee("Vasya", "Pupkin", 2));
            traineeSet.add(new Trainee("Roman", "Elina", 4));
            traineeSet.add(new Trainee("Petr", "Sidorov", 3));

            assertTrue(traineeSet.contains(new Trainee("Nikolay", "Nikolaev", 4)));

            printTraineeSet(traineeSet);

        } catch (TraineeException e) {
            fail("TraineeException");
        }
    }


    @Test
    public void TreeMapTraineeTest() {//17
        Map<Trainee, Institute> traineeInstituteMap = Collections.synchronizedMap(new TreeMap<>());
        try {
            Institute institute1 = new Institute("OMSU", "Omsk");
            Institute institute2 = new Institute("MSU", "Moskow");
            Institute institute3 = new Institute("Hogwarts", "London");

            traineeInstituteMap.put(new Trainee("Nikolay", "Popov", 4), institute1);
            traineeInstituteMap.put(new Trainee("Petr", "Sidorov", 3), institute2);
            traineeInstituteMap.put(new Trainee("Vasya", "Pupkin", 2), institute3);
            traineeInstituteMap.put(new Trainee("Roman", "Elina", 4), institute1);

            assertEquals(traineeInstituteMap.get(new Trainee("Petr", "Sidorov", 3)), institute1);

            printAllTraineeFromInstituteMap(traineeInstituteMap);
            printTraineeFromInstituteMap(traineeInstituteMap);
        } catch (TraineeException e) {
            fail("TraineeException");
        } catch (InstituteException e) {
            fail("InstituteException");
        }
    }

    @Test
    public void TreeMapTraineeTest2() {//18
        Map<Trainee, Institute> traineeInstituteMap = Collections.synchronizedMap(new TreeMap<>());
        try {
            Institute institute1 = new Institute("OMSU", "Omsk");
            Institute institute2 = new Institute("MSU", "Moskow");
            Institute institute3 = new Institute("Hogwarts", "London");

            traineeInstituteMap.put(new Trainee("Petr", "Sidorov", 3), institute2);
            traineeInstituteMap.put(new Trainee("Vasya", "Pupkin", 2), institute3);
            traineeInstituteMap.put(new Trainee("Roman", "Elina", 4), institute1);
            traineeInstituteMap.put(new Trainee("Nikolay", "Popov", 4), institute1);

            assertTrue(traineeInstituteMap.containsKey(new Trainee("Nikolay", "Nikolaev", 4)));

            printAllTraineeFromInstituteMap(traineeInstituteMap);
            printTraineeFromInstituteMap(traineeInstituteMap);
        } catch (TraineeException e) {
            fail("TraineeException");
        } catch (InstituteException e) {
            fail("InstituteException");
        }
    }

    @Test
    public void BitSetTest() {//19
        BitSet bitSet = new BitSet();
        for (int i = 0; i < 10; i++) {
            bitSet.set(i);
        }

        assertTrue(bitSet.get(3));
        bitSet.clear(3);
        assertFalse(bitSet.get(3));
    }


    @Test
    public void timeBitTreeHashSetTest() {//15
        BitSet bitSet = new BitSet();
        long start = nanoTime();
        for (int i = 0; i < 100000; i++) {
            bitSet.set(i);
        }
        long timeBitSet = nanoTime() - start;


        Set<Integer> hashSet = new HashSet<>();
        start = currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            hashSet.add(i);
        }
        long timeHashSet = nanoTime() - start;

        Set<Integer> treeSet = new TreeSet<>();
        start = currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            treeSet.add(i);
        }
        long timeTreeSet = nanoTime() - start;

        System.out.println("Time BitSet= " + timeBitSet);
        System.out.println("Time TreeSet= " + timeTreeSet);
        System.out.println("Time HashSet= " + timeHashSet);

        assertEquals(timeBitSet, timeHashSet);
        assertEquals(timeBitSet, timeTreeSet);
        assertEquals(timeHashSet, timeTreeSet);

    }

    @Test
    public void enumSetTest() {//21
        EnumSet<Color> setAll = EnumSet.allOf(Color.class);
        assertTrue(setAll.contains(Color.BLUE));
    }


    @Test
    public void enumSetTest2() {//21
        EnumSet<Color> setBlue = EnumSet.of(Color.BLUE);
        assertTrue(setBlue.contains(Color.BLUE));
    }

    @Test
    public void enumSetTest3() {//21
        EnumSet<Color> setBetweenGreenAndPink = EnumSet.range(Color.GREEN, Color.PINK);
        assertTrue(setBetweenGreenAndPink.contains(Color.BLUE));
    }

    @Test
    public void enumSetTest4() {//21
        EnumSet<Color> emptySet = EnumSet.noneOf(Color.class);
        assertFalse(emptySet.contains(Color.BLUE));
    }


}
