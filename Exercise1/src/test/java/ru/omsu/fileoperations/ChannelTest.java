package ru.omsu.fileoperations;

import org.junit.Test;
import ru.omsu.trainee.Trainee;
import ru.omsu.trainee.TraineeException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static ru.omsu.fileoperations.FileChannelOperationsTrainee.*;
import static ru.omsu.fileoperations.RenameFilesDirectoryStream.renameFiles;
import static ru.omsu.fileoperations.FileOperationsTrainee.writeToFileOneLine;

public class ChannelTest {

    @Test
    public void readOneTraineeFromChannelTest() {
        try {
            Trainee trainee = new Trainee("Layt", "Terner", 4);
            writeToFileOneLine("input.txt", trainee);
            Trainee trainee1 = readOneTraineeFromChannel("input.txt");
            assertEquals(trainee, trainee1);

        } catch (TraineeException traineeException) {
            fail("TraineeException");
        } catch (FileOperationsException fileOperationsException) {
            fail("FileOperationsException");
        }

    }


    @Test
    public void readOneTraineeFromMappedByteBufferTest() {
        try {
            Trainee trainee = new Trainee("Layt", "Terner", 4);
            writeToFileOneLine("input.txt", trainee);
            Trainee trainee1 = readOneTraineeFromMappedByteBuffer("input.txt");
            assertEquals(trainee, trainee1);

        } catch (TraineeException traineeException) {
            fail("TraineeException");
        } catch (FileOperationsException fileOperationsException) {
            fail("FileOperationsException");
        }
    }

    @Test
    public void writeNumbersWithMappedByteBufferTest() {
        try {
            writeNumbersWithMappedByteBuffer("input.txt");
            byte byteArrayExp[] = new byte[100];
            for (int i = 0; i < 100; i++) {
                byteArrayExp[i] = (byte) i;
            }

            byte[] byteArrayAct = readNumbersWithMappedByteBuffer("input.txt");

            assertEquals(byteArrayExp, byteArrayAct);

        } catch (FileOperationsException fileOperationsException) {
            fail("FileOperationsException");
        }
    }

    @Test
    public void renameFilesTest() {
        try {
            renameFiles("D:\\SomeDir");
        } catch (FileOperationsException e) {
            fail("FileOperationsException");
        }
    }

    @Test
    public void serializationByteBufferTest() {
        try {
            Trainee trainee = new Trainee("Layt", "Terner", 4);
            byte[] buf = serializationByteBuffer(trainee);
            Trainee trainee1 = deserializationByteBuffer(buf);
            assertEquals(trainee, trainee1);
        } catch (TraineeException traineeException) {
            fail("TraineeException");
        } catch (FileOperationsException fileOperationsException) {
            fail("FileOperationsException");
        }
    }

}
