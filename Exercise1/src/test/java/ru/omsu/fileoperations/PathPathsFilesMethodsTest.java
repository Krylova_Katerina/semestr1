package ru.omsu.fileoperations;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static junit.framework.TestCase.*;
import static org.junit.Assert.fail;

public class PathPathsFilesMethodsTest {

    @Test
    public void isAbsoluteTest() {
        Path path = Paths.get("D:\\CiPPO\\semestr1\\Exercise1\\input.txt");
        assertTrue(path.isAbsolute());

    }

    @Test
    public void isAbsoluteTest2() {
        Path path = Paths.get("input.txt");
        assertFalse(path.isAbsolute());

    }

    @Test
    public void getFileNameTest() {
        Path path = Paths.get("D:\\CiPPO\\semestr1\\Exercise1\\input.txt");
        System.out.println(path.getFileName());
        assertEquals(path.getFileName(), "input.txt");
    }

    @Test
    public void getParentTest() {
        Path path = Paths.get("D:\\CiPPO\\semestr1\\Exercise1\\input.txt");
        assertEquals(path.getParent(), "D:\\CiPPO\\semestr1\\Exercise1");
    }

    @Test
    public void getRootTest() {
        Path path = Paths.get("D:\\CiPPO\\semestr1\\Exercise1\\input.txt");
        assertEquals(path.getRoot(), "D:\\");

    }

    @Test
    public void createDirectoryTest() {
        Path path = Paths.get("D:\\SomeDir");
        try {
            Files.createDirectory(path);
        } catch (IOException e) {
            fail("IOException");
        }
    }

    @Test
    public void createFileTest() {
        Path path = Paths.get("D:\\SomeDir\\input.txt");
        try {
            Files.createFile(path);
        } catch (IOException e) {
            fail("IOException");
        }
    }

    @Test
    public void deleteFileTest() {
        Path path = Paths.get("D:\\SomeDir\\input.txt");
        try {
            Files.delete(path);
        } catch (IOException e) {
            fail("IOException");
        }
    }

    @Test
    public void deleteDirectoryTest() {
        Path path = Paths.get("D:\\SomeDir");
        try {
            Files.delete(path);
        } catch (IOException e) {
            fail("IOException");
        }
    }

}
