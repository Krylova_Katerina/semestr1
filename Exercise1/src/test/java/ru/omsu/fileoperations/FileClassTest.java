package ru.omsu.fileoperations;

import org.junit.Test;
import ru.omsu.trainee.TraineeException;

import java.io.File;
import java.io.IOException;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;


/**
 * Created by User on 11.09.2017.
 */


public class FileClassTest {

    @Test
    public void createDirTest() {
        File dir = new File("NewDir");
        boolean created = dir.mkdir();
        assertTrue(created);

    }

    @Test
    public void deleteDirTest() {
        File dir = new File("NewDir");
        boolean created = dir.mkdir();
        boolean deleted = dir.delete();
        System.out.print(deleted);
        assertTrue(deleted);
    }

    @Test
    public void renameDirTest() {
        File dir = new File("NewDir");
        boolean created = dir.mkdir();
        File newDir = new File("NewDirRenamed");
        dir.renameTo(newDir);
        assertTrue(dir.exists());
    }


    @Test
    public void createFileTest() throws IOException {
        File file1 = new File("SomeDir", "Hello.txt");
        boolean created = file1.createNewFile();
        assertTrue(created);

    }


    @Test
    public void deleteFileTest() throws TraineeException, IOException {
        File file1 = new File("SomeDir", "Good.txt");
        boolean created = file1.createNewFile();
        boolean deleted = file1.delete();
        assertTrue(deleted);
    }

    @Test
    public void renameFileTest() throws TraineeException, IOException {
        File file1 = new File("SomeDir", "FoodGood.txt");
        boolean created = file1.createNewFile();
        File newDir = new File("NewGod.txt");
        file1.renameTo(newDir);
        assertTrue(file1.exists());
    }


    @Test
    public void absolutNameFileTest() throws TraineeException, IOException {
        File file1 = new File("SomeDir", "FoodGood.txt");
        boolean created = file1.createNewFile();
        String train = file1.getAbsolutePath();
        assertEquals("FoodGood.txt", train);
    }

    @Test
    public void isFileTest() throws TraineeException, IOException {
        File file1 = new File("SomeDir", "FoodGood.txt");
        boolean created = file1.createNewFile();
        assertTrue(file1.isFile());
    }

    @Test
    public void hasFileTest() throws TraineeException, IOException {
        File file1 = new File("SomeDir", "FoodGood.txt");
        boolean created = file1.createNewFile();
        assertTrue(file1.exists());
    }


    @Test
    public void arrFilesTest() throws TraineeException, IOException {
        File file1 = new File("SomeDir", "FoodGood.txt");
        boolean created = file1.createNewFile();
        File[] arrFiles = file1.listFiles();
    }

}
