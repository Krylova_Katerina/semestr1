package ru.omsu.trainee;

import org.junit.Test;
import ru.omsu.fileoperations.FileOperationsException;

import java.io.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static ru.omsu.fileoperations.FileOperationsTrainee.*;

/**
 * Created by User on 06.09.2017.
 */
public class TraineeTest {
    @Test
    public void correctDataTest() throws TraineeException {
        Trainee trainee = new Trainee("Vasya", "Vasilev", 4);
        Trainee trainee2 = new Trainee("Elena", "Vasileva", 3);
        Trainee trainee3 = new Trainee("Vasilisa", "Maricheva", 5);
    }

    @Test(expected = TraineeException.class)
    public void incorrectNameTest() throws TraineeException {
        Trainee trainee = new Trainee("", "Vasilev", 4);
    }

    @Test(expected = TraineeException.class)
    public void incorrectNameTest2() throws TraineeException {
        Trainee trainee = new Trainee(null, "Vasilev", 4);
    }

    @Test(expected = TraineeException.class)
    public void incorrectLastNameTest() throws TraineeException {
        Trainee trainee = new Trainee("Vasya", "", 4);
    }


    @Test(expected = TraineeException.class)
    public void incorrectLastNameTest2() throws TraineeException {
        Trainee trainee = new Trainee("Vasya", null, 4);
    }

    @Test(expected = TraineeException.class)
    public void incorrectRatingTest() throws TraineeException {
        Trainee trainee = new Trainee("Vasya", "Vasya", 18);
    }

    @Test(expected = TraineeException.class)
    public void incorrectRatingTest2() throws TraineeException {
        Trainee trainee = new Trainee("Vasya", "Vasya", -3);
    }

    @Test(expected = TraineeException.class)
    public void incorrectChangesTest() throws TraineeException {
        Trainee trainee = new Trainee("Vasya", "Vasya", 4);
        trainee.setRating(18);
    }

    @Test(expected = TraineeException.class)
    public void incorrectChangesTest2() throws TraineeException {
        Trainee trainee = new Trainee("Vasya", "Vasya", 4);
        trainee.setLastName("");
    }

    @Test
    public void writeToFileTest() {
        try {
            Trainee trainee = new Trainee("Bob", "Bobovych", 4);
            writeToFile("input.txt", trainee);

            Trainee newTrainee = readFromFile("input.txt");
            assertEquals(trainee, newTrainee);
        } catch (FileOperationsException fileOperationsException) {
            fail("FileOperationsException");
        } catch (IOException e) {
            fail("IOException");
        } catch (TraineeException traineeException) {
            fail("TraineeException");
        }
    }

    @Test(expected = FileOperationsException.class)
    public void readFromFileTest2() throws TraineeException, IOException, FileOperationsException {
        Trainee trainee = new Trainee("Bob", "Bobovych", 4);
        Trainee newTrainee = readFromFile("inputWrong.txt");
        assertEquals(trainee, newTrainee);
    }


    @Test(expected = FileOperationsException.class)
    public void readFromFileOneLineTest() throws TraineeException, IOException, FileOperationsException {
        Trainee trainee = new Trainee("Layt", "Terner", 4);
        Trainee newTrainee = readFromFileOneLine("inputWrong.txt");
        assertEquals(trainee, newTrainee);
    }


    @Test
    public void writeToFileOneLineTest1() {
        try {
            Trainee trainee = new Trainee("Layt", "Terner", 4);
            writeToFileOneLine("input.txt", trainee);
            Trainee newTrainee = readFromFileOneLine("input.txt");
            assertEquals(trainee, newTrainee);
        } catch (FileOperationsException fileOperationsException) {
            fail("FileOperationsException");
        } catch (TraineeException traineeException) {
            fail("TraineeException");
        }

    }

    @Test
    public void serializationTest() {
        try {
            Trainee trainee = new Trainee("Vasya", "Tort", 4);
            serialization(trainee, "input.txt");

            Trainee newTrainee = deserialization("input.txt");
            assertEquals(trainee, newTrainee);
        } catch (FileOperationsException fileOperationsException) {
            fail("FileOperationsException");
        } catch (TraineeException traineeException) {
            fail("TraineeException");
        }
    }

    @Test
    public void serializationToBinaryFileTest() {
        try {
            Trainee trainee = new Trainee("Vasya", "V", 4);
            byte[] buffer = null;
            try (ByteArrayOutputStream outArr = new ByteArrayOutputStream();
                 ObjectOutputStream out = new ObjectOutputStream(outArr)) {
                out.writeObject(trainee);
                buffer = outArr.toByteArray();
            } catch (IOException e) {
                fail("IOException");
            }

            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(buffer))) {
                Trainee newTrainee = (Trainee) in.readObject();
                assertEquals(trainee, newTrainee);
            } catch (ClassCastException e) {
                fail("ClassCastException");
            } catch (ClassNotFoundException e) {
                fail("ClassNotFoundException");
            } catch (IOException e) {
                fail("IOException");
            }
        } catch (TraineeException traineeException) {
            fail("TraineeException");
        }

    }

    @Test
    public void serializationToJsonTest() {
        try {
            Trainee trainee = new Trainee("Vasya", "Vlad", 4);
            serializationToJsonFile(trainee, "input.txt");

            Trainee newTrainee = deserializationFromJsonFile("input.txt");
            assertEquals(trainee, newTrainee);
        } catch (FileOperationsException fileOperationsException) {
            fail("FileOperationsException");
        } catch (TraineeException traineeException) {
            fail("TraineeException");
        }

    }

    @Test
    public void serializationToJsonTestFile() {
        try {
            Trainee trainee = new Trainee("Vasya", "Vl", 4);
            serializationToJson(trainee, "input.txt");

            Trainee newTrainee = deserializationFromJson("input.txt");
            assertEquals(trainee, newTrainee);

        } catch (FileOperationsException fileOperationsException) {
            fail("FileOperationsException");
        } catch (TraineeException traineeException) {
            fail("TraineeException");
        }

    }


}


