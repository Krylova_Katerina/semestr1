package ru.omsu.rectangle;

import org.junit.Test;
import ru.omsu.fileoperations.FileOperationsException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static ru.omsu.fileoperations.FileOperationsRectangle.*;

public class RectangleTest {


    @Test
    public void writeToFileTest2() {
        Rectangle rectangle = new Rectangle(12232, 53221, 32344, 312314);
        try {
            writeToFile("input.txt", rectangle);
            Rectangle newRectangle = readFromFile("input.txt");
            assertEquals(rectangle, newRectangle);
        } catch (FileOperationsException fileOperationsException) {
            fail("FileOperationsException");
        }

    }


    @Test
    public void readWriteFiveRectangleTest() {
        try {
            List<Rectangle> rectangleList = new ArrayList<>();
            rectangleList.add(new Rectangle(1234, 5213, 3123, 3234));
            rectangleList.add(new Rectangle(2234, 5233, 3213, 3574));
            rectangleList.add(new Rectangle(3234, 6325, 4345, 3345));
            rectangleList.add(new Rectangle(4234, 7523, 5235, 3234));
            rectangleList.add(new Rectangle(5453, 8235, 6345, 3123));

            writeToFileNRectangles("input.txt", rectangleList);
            List<Rectangle> newRectangleList = readFromFileFiveRectangles("input.txt");

            List<Rectangle> expectedList = new ArrayList<>();
            expectedList.add(new Rectangle(5453, 8235, 6345, 3123));
            expectedList.add(new Rectangle(4234, 7523, 5235, 3234));
            expectedList.add(new Rectangle(3234, 6325, 4345, 3345));
            expectedList.add(new Rectangle(2234, 5233, 3213, 3574));
            expectedList.add(new Rectangle(1234, 5213, 3123, 3234));


            assertEquals(expectedList, newRectangleList);


        } catch (FileOperationsException fileOperationsException) {
            fail("FileOperationsException");

        }
    }
}




