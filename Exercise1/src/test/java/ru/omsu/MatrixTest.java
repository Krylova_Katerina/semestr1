package ru.omsu;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static ru.omsu.Matrix.searchSimilarStrings;

public class MatrixTest {

    @Test
    public void matrixTest() {//22
        String[] matrix = new String[]{"1 2 3", "3 2 1", "4 2 1"};
        Set<Set<String>> result = searchSimilarStrings(matrix);

        Set<Set<String>> act = new HashSet<>();
        Set<String> nextLine = new HashSet<>(Arrays.asList(matrix[1].split(" ")));
        act.add(nextLine);
        nextLine = new HashSet<>(Arrays.asList(matrix[2].split(" ")));
        act.add(nextLine);

        assertEquals(result, act);
    }


    @Test
    public void matrixTest2() {//22
        String[] matrix = new String[]{"1 2 2 3", "3 3 2 1", "4 4 2 1"};
        Set<Set<String>> result = searchSimilarStrings(matrix);

        Set<Set<String>> act = new HashSet<>();
        Set<String> nextLine = new HashSet<>(Arrays.asList(matrix[1].split(" ")));
        act.add(nextLine);
        nextLine = new HashSet<>(Arrays.asList(matrix[2].split(" ")));
        act.add(nextLine);

        assertEquals(result, act);
    }
}
