package ru.omsu.group;

import org.junit.Test;
import ru.omsu.trainee.Trainee;
import ru.omsu.trainee.TraineeException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static ru.omsu.group.Group.sortingByFirstName;
import static ru.omsu.group.Group.sortingByRaiting;

//import static ru.omsu.group.group.searchByName;

public class GroupTest {
    @Test
    public void correctDataTest() {
        try {
            Trainee[] trainees = {new Trainee("Vasya", "Popov", 4),
                    new Trainee("Petr", "Sidorov", 4),
                    new Trainee("Vasya", "Pupkin", 4),
                    new Trainee("Roman", "Elina", 4)};

            Group group = new Group("programmers", trainees);
        } catch (TraineeException e) {
            fail("TraineeException");
        } catch (GroupException e) {
            fail("GroupException");
        }
    }

    @Test(expected = GroupException.class)
    public void incorrectNameTest() throws TraineeException, GroupException {
        Trainee[] trainees = {new Trainee("Vasya", "Popov", 4),
                new Trainee("Petr", "Sidorov", 4),
                new Trainee("Vasya", "Pupkin", 4),
                new Trainee("Roman", "Elina", 4)};

        Group group = new Group(null, trainees);
    }

    @Test(expected = GroupException.class)
    public void incorrectNameTest2() throws TraineeException, GroupException {
        Trainee[] trainees = {new Trainee("Vasya", "Popov", 4),
                new Trainee("Petr", "Sidorov", 4),
                new Trainee("Vasya", "Pupkin", 4),
                new Trainee("Roman", "Elina", 4)};

        Group group = new Group("", trainees);
    }

    @Test(expected = GroupException.class)
    public void incorrectNameTest3() throws GroupException {
        Group group = new Group("", null);
    }

    /* @Test
       public void searchByNameTest() {
           try {
               trainee[] trainees = {new trainee("Vasya", "Popov", 4),
                       new trainee("Petr", "Sidorov", 4),
                       new trainee("Vasya", "Pupkin", 4),
                       new trainee("Roman", "Elina", 4)};

               group group = new group("Programmers", trainees);
               trainee result = searchByName(group, "Petr");
               assertEquals(result, new trainee("Petr", "Sidorov", 4));
           } catch (TraineeException e) {
               fail("TraineeException");
           } catch (GroupException e) {
               fail("GroupException");
           }
       }

   */
    @Test
    public void sortByFirstNameTest() {
        try {
            Trainee[] trainees = {new Trainee("Vasya", "Popov", 4),
                    new Trainee("Petr", "Sidorov", 4),
                    new Trainee("Vasya", "Pupkin", 4),
                    new Trainee("Roman", "Elina", 4)};

            Group group = new Group("programmers", trainees);
            sortingByFirstName(group);

            Trainee[] trainees2 = {
                    new Trainee("Petr", "Sidorov", 4),
                    new Trainee("Roman", "Elina", 4),
                    new Trainee("Vasya", "Popov", 4),
                    new Trainee("Vasya", "Pupkin", 4)
            };

            Group expGroup = new Group("programmers", trainees2);
            assertEquals(group, expGroup);
        } catch (TraineeException e) {
            fail("TraineeException");
        } catch (GroupException e) {
            fail("GroupException");
        }
    }

    @Test
    public void sortByRatingTest() {
        try {
            Trainee[] trainees = {new Trainee("Vasya", "Popov", 5),
                    new Trainee("Petr", "Sidorov", 2),
                    new Trainee("Vasya", "Pupkin", 3),
                    new Trainee("Roman", "Elina", 4)};

            Group group = new Group("programmers", trainees);
            sortingByRaiting(group);

            Trainee[] trainees2 = {
                    new Trainee("Petr", "Sidorov", 2),
                    new Trainee("Vasya", "Pupkin", 3),
                    new Trainee("Roman", "Elina", 4),
                    new Trainee("Vasya", "Popov", 5)};

            Group expGroup = new Group("programmers", trainees2);
            assertEquals(group, expGroup);
        } catch (TraineeException e) {
            fail("TraineeException");
        } catch (GroupException e) {
            fail("GroupException");
        }
    }
}