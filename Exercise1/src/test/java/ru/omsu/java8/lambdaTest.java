package ru.omsu.java8;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public class lambdaTest {

    @Test
    public void splitTest() {//1
        Function<String, List<String>> f1 = s -> Arrays.asList(s.split(" "));
        List<String> exp = f1.apply("Hello I'm Groot !");
        List<String> result = new ArrayList<>();
        result.add("Hello");
        result.add("I'm");
        result.add("Groot");
        result.add("!");
        assertEquals(result, exp);
         }

    @Test
    public void countTest2() {//2
        Function<List<String>, Integer> f1 = s -> s.size();
        List<String> result = new ArrayList<>();
        result.add("Hello");
        result.add("I'm");
        result.add("Groot");
        result.add("!");
        Integer exp = f1.apply(result);
        Integer res = 4;
        assertEquals(res, exp);
    }

    @Test
    public void countAndSplitTest2() {//4 and 1
        Function<String, List<String>> f1 = s -> Arrays.asList(s.split(" "));

        Function<List<String>, Integer> f2 = s -> s.size();

        Integer exp = f1.andThen(f2).apply("Hello I'm Groot !");
        Integer exp2 = f2.compose(f1).apply("Hello I'm Groot !");
        assertEquals(exp2, exp);

    }


    @Test
    public void maxTest() {//6
        BiFunction<Double, Double, Double> f1 = Math::max;
        assertEquals(7.7,f1.apply(3.3, 7.7));

    }

    @Test
    public void getCurrentDateTest() {//7
        Supplier<Date> f1= Date::new;
        System.out.println(f1.get());

    }

    @Test
    public void isEvenTest() {//8
        Predicate<Integer> f1 = s -> s%2 == 0;
        assertTrue(f1.test(4));

    }

    @Test
    public void areEqualTest() {//9
        BiFunction<Integer, Integer, Boolean> f1 = (a,b) -> a.equals(b);
        assertTrue(f1.apply(5,5));

    }


}
