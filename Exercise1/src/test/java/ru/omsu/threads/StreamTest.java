package ru.omsu.threads;

import org.junit.Test;
import ru.omsu.threads.threadsforarraylist.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.Thread.activeCount;
import static java.lang.Thread.currentThread;
import static org.junit.Assert.fail;

public class StreamTest {

    @Test
    public void streamDemoTest() {//2
        try {
            ThreadOne one = new ThreadOne("Stream");
            one.getT().join();
        } catch (InterruptedException e) {
            fail("Main thread interrupted.");
        }
        System.out.println("Main thread exiting.");
    }


    @Test
    public void streamDemoTest2() {//1
        try {
            Thread ob1 = new Thread("Stream");

            System.out.println(activeCount());
            System.out.println(currentThread());
            Thread.sleep(500);
            System.out.println(ob1.isAlive());
            System.out.println(ob1.getName());
            System.out.println(ob1.getId());
            System.out.println(ob1.isDaemon());
            System.out.println(ob1.getPriority());
        } catch (InterruptedException e) {
            fail("InterruptedException");
        }
    }

    @Test
    public void streamsTest() {//3

        ThreadOne one = new ThreadOne("One");
        ThreadTwo two = new ThreadTwo("Two");
        ThreadThree three = new ThreadThree("Three");
        try {
            one.getT().join();
            two.getT().join();
            three.getT().join();
        } catch (InterruptedException e) {
            fail("Main thread interrupted.");
        }
        System.out.println("Main thread exiting.");
    }

    @Test
    public void streamsArrayListTest() {//4
        List<Integer> list = new ArrayList<>();
        ThreadOneForArrayList one = new ThreadOneForArrayList("One", list);
        ThreadTwoForArrayList two = new ThreadTwoForArrayList("Two", list);

        System.out.println("Main thread exiting.");
    }


    @Test
    public void streamArrayListTest() {//5
        List<Integer> list = new ArrayList<>();
        ThreadThreeForArrayList one = new ThreadThreeForArrayList("One", list, true);
        ThreadThreeForArrayList two = new ThreadThreeForArrayList("Two", list, false);

        System.out.println("Main thread exiting.");
    }

    @Test
    public void streamArrayListReentrantLockTest() {//10
        List<Integer> list = new ArrayList<>();
        ReentrantLock lock = new ReentrantLock();
        ThreadForALOnBaseReentrantLock one = new ThreadForALOnBaseReentrantLock(lock, "One", list, true);
        ThreadForALOnBaseReentrantLock two = new ThreadForALOnBaseReentrantLock(lock, "Two", list, false);
    }

    @Test
    public void streamArrayListTest2() {//6
        List<Integer> list = new ArrayList<>();
        ThreadFourForArrayList one = new ThreadFourForArrayList("One", Collections.synchronizedList(list), true);
        ThreadFourForArrayList two = new ThreadFourForArrayList("Two", Collections.synchronizedList(list), false);

        System.out.println("Main thread exiting.");
    }


}
