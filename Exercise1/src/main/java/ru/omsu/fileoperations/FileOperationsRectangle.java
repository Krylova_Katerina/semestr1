package ru.omsu.fileoperations;

import ru.omsu.rectangle.Rectangle;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 18.09.2017.
 */

public class FileOperationsRectangle {

    public static Rectangle readFromFile(String fileName) throws FileOperationsException {
        Rectangle rectangle = new Rectangle();
        try (DataInputStream in = new DataInputStream(new FileInputStream(fileName))) {
            rectangle.setLeft(in.readDouble());
            rectangle.setTop(in.readDouble());
            rectangle.setRight(in.readDouble());
            rectangle.setBottom(in.readDouble());

        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e, FileOperationsErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }
        return rectangle;
    }

    public static void writeToFile(String fileName, Rectangle rectangle) throws FileOperationsException {
        try (DataOutputStream out = new DataOutputStream(new FileOutputStream(fileName))) {
            out.writeDouble(rectangle.getLeft());
            out.writeDouble(rectangle.getTop());
            out.writeDouble(rectangle.getRight());
            out.writeDouble(rectangle.getBottom());
        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e, FileOperationsErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }
    }


    public static void writeToFileNRectangles(String fileName, List<Rectangle> rectangleList) throws FileOperationsException {
        try (RandomAccessFile data = new RandomAccessFile(new File(fileName), "rw")) {
            for (Rectangle i : rectangleList) {
                data.writeDouble(i.getLeft());
                data.writeDouble(i.getTop());
                data.writeDouble(i.getRight());
                data.writeDouble(i.getBottom());
            }
        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e, FileOperationsErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }
    }

    public static List<Rectangle> readFromFileFiveRectangles(String fileName) throws FileOperationsException {
        List<Rectangle> newRectangleList = new ArrayList<>();
        try (RandomAccessFile data = new RandomAccessFile(new File(fileName), "r")) {
            for (int k = 0; k < 5; k++) {
                data.seek((4 - k) * 4 * Double.BYTES);
                newRectangleList.add(new Rectangle(data.readDouble(), data.readDouble(), data.readDouble(), data.readDouble()));
            }
        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e, FileOperationsErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }
        return newRectangleList;
    }
}
