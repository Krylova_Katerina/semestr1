package ru.omsu.fileoperations;

import com.google.gson.Gson;
import ru.omsu.trainee.Trainee;
import ru.omsu.trainee.TraineeErrorCodes;
import ru.omsu.trainee.TraineeException;

import java.io.*;

/**
 * Created by User on 18.09.2017.
 */
public class FileOperationsTrainee {

    public static Trainee readFromFileOneLine(String fileName) throws FileOperationsException, TraineeException {//12
        Trainee trainee = new Trainee();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String[] ostr = br.readLine().split(" ");
            trainee.setFirstName(ostr[0]);
            trainee.setLastName(ostr[1]);
            try {
                trainee.setRating(Integer.valueOf(ostr[2]));
            } catch (NumberFormatException e) {
                throw new TraineeException(e, TraineeErrorCodes.WRONG_RATING, ostr[2]);
            }

        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e, FileOperationsErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        } catch (TraineeException traineeException) {
            throw new TraineeException(traineeException);
        }
        return trainee;
    }


    public static Trainee readFromFile(String fileName) throws IOException, FileOperationsException, TraineeException {//11
        Trainee trainee = new Trainee();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            trainee.setFirstName(br.readLine());
            trainee.setLastName(br.readLine());
            String str = br.readLine();
            try {
                trainee.setRating(Integer.valueOf(str));
            } catch (NumberFormatException e) {
                throw new TraineeException(e, TraineeErrorCodes.WRONG_RATING, str);
            }

        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e, FileOperationsErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        } catch (TraineeException traineeException) {
            throw new TraineeException(traineeException);
        }
        return trainee;
    }


    public static void writeToFile(String fileName, Trainee trainee) throws FileOperationsException {//11
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
            bw.write(trainee.getFirstName());
            bw.newLine();
            bw.write(trainee.getLastName());
            bw.newLine();
            bw.write(String.valueOf(trainee.getRating()));
        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e, FileOperationsErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }

    }

    public static void writeToFileOneLine(String fileName, Trainee trainee) throws FileOperationsException {//13
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
            bw.write(trainee.getFirstName());
            bw.write(" ");
            bw.write(trainee.getLastName());
            bw.write(" ");
            bw.write(String.valueOf(trainee.getRating()));
        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e, FileOperationsErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }
    }


    public static void serialization(Trainee trainee, String fileName) throws FileOperationsException {//15
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName))) {
            out.writeObject(trainee);
        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e, FileOperationsErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }
    }

    public static Trainee deserialization(String fileName) throws FileOperationsException {//15
        Trainee trainee = new Trainee();
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName))) {
            trainee = (Trainee) in.readObject();

        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e, FileOperationsErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        } catch (ClassNotFoundException e) {
            throw new FileOperationsException(e, FileOperationsErrorCodes.CLASS_NOT_FOUND);
        }
        return trainee;
    }

    public static void serializationToJsonFile(Trainee trainee, String fileName) throws FileOperationsException {//17
        try (FileWriter writer = new FileWriter(fileName)) {
            Gson gson = new Gson();
            gson.toJson(trainee, writer);
        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e, FileOperationsErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }
    }

    public static Trainee deserializationFromJsonFile(String fileName) throws FileOperationsException {//17

        Gson gson = new Gson();
        try (Reader reader = new FileReader(fileName)) {
            Trainee trainee = gson.fromJson(reader, Trainee.class);
            return trainee;
        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e, FileOperationsErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }
    }

    public static void serializationToJson(Trainee trainee, String fileName) throws FileOperationsException {//18
        Gson gson = new Gson();
        String jsonTranee = gson.toJson(trainee);
        try (FileWriter writer = new FileWriter(fileName)) {
            writer.write(jsonTranee);
        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e, FileOperationsErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }
    }

    public static Trainee deserializationFromJson(String fileName) throws FileOperationsException {//18
        Trainee trainee = new Trainee();
        Gson gson = new Gson();
        String readString = null;
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            readString = reader.readLine();
            trainee = gson.fromJson(readString, Trainee.class);

        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e, FileOperationsErrorCodes.FILE_NOT_FOUND);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }

        return trainee;
    }
}
