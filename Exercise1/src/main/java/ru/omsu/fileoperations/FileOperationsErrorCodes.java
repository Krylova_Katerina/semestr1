package ru.omsu.fileoperations;

/**
 * Created by User on 18.09.2017.
 */
enum FileOperationsErrorCodes {
    FILE_NOT_FOUND("File not found "),
    CLASS_NOT_FOUND("Class not found");
    private String message;

    private FileOperationsErrorCodes(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

