package ru.omsu.fileoperations;

/**
 * Created by User on 06.09.2017.
 */
public class FileOperationsException extends Exception {


    public FileOperationsException(Throwable cause, FileOperationsErrorCodes message) {
        super(message.getMessage(), cause);
    }

    public FileOperationsException(Throwable cause, String message) {
        super(message, cause);
    }

    public FileOperationsException(Throwable cause) {
        super(cause);
    }
}

