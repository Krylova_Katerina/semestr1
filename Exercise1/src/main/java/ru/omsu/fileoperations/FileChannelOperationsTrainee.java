package ru.omsu.fileoperations;

import ru.omsu.trainee.Trainee;
import ru.omsu.trainee.TraineeErrorCodes;
import ru.omsu.trainee.TraineeException;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class FileChannelOperationsTrainee {

    public static Trainee readOneTraineeFromChannel(String fileName) throws FileOperationsException, TraineeException {
        Trainee trainee = new Trainee();
        try (FileChannel channel = new FileInputStream(new File(fileName)).getChannel()) {
            ByteBuffer buffer = ByteBuffer.allocate((int) channel.size());
            channel.read(buffer);
            trainee = bufferToTrainee(buffer);
        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }
        return trainee;
    }

    public static Trainee bufferToTrainee(ByteBuffer buffer) throws TraineeException {
        Trainee trainee = new Trainee();
        byte[] byteArray = buffer.array();
        String value = null;
        try {
            value = new String(byteArray, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new TraineeException(e);
        }

        String[] ostr = value.split(" ");
        try {
            trainee.setFirstName(ostr[0]);
            trainee.setLastName(ostr[1]);
            try {
                trainee.setRating(Integer.valueOf(ostr[2]));
            } catch (NumberFormatException e) {
                throw new TraineeException(e, TraineeErrorCodes.WRONG_RATING, ostr[2]);
            }
        } catch (TraineeException traineeException) {
            throw new TraineeException(traineeException);
        }
        return trainee;

    }

    public static Trainee readOneTraineeFromMappedByteBuffer(String fileName) throws FileOperationsException, TraineeException {
        Trainee trainee = new Trainee();
        try (FileChannel channel = new RandomAccessFile(new File(fileName), "rw").getChannel()) {
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, channel.size());
            trainee = bufferToTrainee(buffer);
        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }
        return trainee;
    }


    public static void writeNumbersWithMappedByteBuffer(String fileName) throws FileOperationsException {
        try (FileChannel channel = new RandomAccessFile(new File(fileName), "rw").getChannel()) {
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, channel.size());
            for (int i = 0; i < 100; i++) {
                buffer.put((byte) i);
            }
        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }

    }


    public static byte[] readNumbersWithMappedByteBuffer(String fileName) throws FileOperationsException {
        byte[] byteArray = new byte[100];
        try (FileChannel channel = new RandomAccessFile(new File(fileName), "rw").getChannel()) {
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, channel.size());
            buffer.position(0);
            for (int i = 0; i < buffer.capacity(); i++) {
                byteArray[i] = buffer.get();
            }
        } catch (FileNotFoundException e) {
            throw new FileOperationsException(e);
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }
        return byteArray;
    }

    public static byte[] serializationByteBuffer(Trainee trainee) throws FileOperationsException {
        byte[] bufferArray;
        try (ByteArrayOutputStream outArr = new ByteArrayOutputStream();
             ObjectOutputStream out = new ObjectOutputStream(outArr)) {
            out.writeObject(trainee);
            bufferArray = outArr.toByteArray();
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }
        return bufferArray;
    }

    public static Trainee deserializationByteBuffer(byte[] buffer) throws FileOperationsException {
        Trainee trainee = new Trainee();
        try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(buffer))) {
            trainee = (Trainee) in.readObject();
        } catch (IOException e) {
            throw new FileOperationsException(e);
        } catch (ClassNotFoundException e) {
            throw new FileOperationsException(e);
        }
        return trainee;
    }

}

