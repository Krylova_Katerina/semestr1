package ru.omsu.fileoperations;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class RenameFilesDirectoryStream {

    public static void renameFiles(String directoryName) throws FileOperationsException {
        Path pathSomeDirectory = Paths.get(directoryName);
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(pathSomeDirectory, "*.dat")) {
            for (Path path : stream) {
                String oldName = path.getFileName().toFile().getName();
                String newName = oldName.replaceFirst(".dat", ".bin");
                Path source = Paths.get(directoryName + "\\" + oldName);
                Files.move(source, source.resolveSibling(newName));
            }
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }
    }

}
