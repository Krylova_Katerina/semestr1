package ru.omsu.trainee;

/**
 * Created by User on 06.09.2017.
 */
public enum TraineeErrorCodes {
    WRONG_FIRSTNAME("Wrong first name %s"),
    WRONG_LASTNAME("Wrong last name %s"),
    WRONG_RATING("Wrong rating %s");

    private String message;

    private TraineeErrorCodes(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}