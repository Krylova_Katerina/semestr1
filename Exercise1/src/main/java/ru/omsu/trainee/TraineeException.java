package ru.omsu.trainee;

/**
 * Created by User on 06.09.2017.
 */
public class TraineeException extends Exception {

    public TraineeException(TraineeErrorCodes errorCode, String param) {
        super(String.format(errorCode.getMessage(), param));
    }

    public TraineeException(TraineeErrorCodes errorCode, int param) {
        super(String.format(errorCode.getMessage(), param));
    }

    public TraineeException(Throwable cause) {
        super(cause);
    }

    public TraineeException(NumberFormatException e, TraineeErrorCodes wrongRating, String ostr) {
        super((String.format(wrongRating.getMessage(), ostr)), e);
    }
}

