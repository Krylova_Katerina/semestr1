package ru.omsu.trainee;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by User on 06.09.2017.
 */
public class Trainee implements Serializable, Comparable<Trainee> {
    private String firstName;
    private String lastName;
    private int rating;

    public Trainee() {
    }

    public Trainee(String firstName, String lastName, int rating) throws TraineeException {
        setFirstName(firstName);
        setLastName(lastName);
        setRating(rating);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) throws TraineeException {
        if (firstName == null || firstName.length() == 0)
            throw new TraineeException(TraineeErrorCodes.WRONG_FIRSTNAME, firstName);

        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) throws TraineeException {
        if (lastName == null || lastName.length() == 0)
            throw new TraineeException(TraineeErrorCodes.WRONG_LASTNAME, lastName);
        this.lastName = lastName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) throws TraineeException {
        if (rating < 1 || rating > 5)
            throw new TraineeException(TraineeErrorCodes.WRONG_RATING, rating);
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Trainee trainee = (Trainee) o;

        if (rating != trainee.rating) return false;
        if (firstName != null ? !firstName.equals(trainee.firstName) : trainee.firstName != null) return false;
        return lastName != null ? lastName.equals(trainee.lastName) : trainee.lastName == null;

    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + rating;
        return result;
    }

    @Override
    public int compareTo(Trainee trainee) {
        return firstName.compareTo(trainee.getFirstName());
    }

    @Override
    public String toString() {
        return "trainee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", rating=" + rating +
                '}';
    }

    public static void printTraineeSet(Set<Trainee> traineeSet) {
        for (Trainee trainee : traineeSet) {
            System.out.println(trainee.toString());
        }
    }
}


