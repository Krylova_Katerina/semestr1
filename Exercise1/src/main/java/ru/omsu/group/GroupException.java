package ru.omsu.group;

import ru.omsu.trainee.Trainee;

public class GroupException extends Throwable {


    public GroupException(GroupErrorCodes wrongGroupName, String groupName) {
        super(String.format(wrongGroupName.getMessage(), groupName));
    }

    public GroupException(GroupErrorCodes wrongTrainees, Trainee[] trainees) {
        super(String.format(wrongTrainees.getMessage(), trainees));
    }

    public GroupException(GroupErrorCodes traineeNotFound) {
        super(String.format(traineeNotFound.getMessage()));
    }
}
