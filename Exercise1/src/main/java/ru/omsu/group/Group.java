package ru.omsu.group;

import ru.omsu.trainee.Trainee;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Group {
    private String groupName;
    private Trainee[] trainees;

    public Group(String groupName, Trainee[] trainees) throws GroupException {
        setTrainees(trainees);
        setGroupName(groupName);
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) throws GroupException {
        if (groupName == null || groupName.length() == 0)
            throw new GroupException(GroupErrorCodes.WRONG_GROUPNAME, groupName);
        this.groupName = groupName;
    }

    public Trainee[] getTrainees() {
        return trainees;
    }

    public void setTrainees(Trainee[] trainees) throws GroupException {
        if (trainees == null || trainees.length == 0) {
            throw new GroupException(GroupErrorCodes.WRONG_TRAINEES, trainees);
        }
        this.trainees = trainees;
    }

    public static Group sortingByRaiting(Group group) throws GroupException {
        List<Trainee> list = Arrays.asList(group.getTrainees());
        Collections.sort(list, new Comparator<Trainee>() {
            @Override
            public int compare(Trainee o1, Trainee o2) {
                return o1.getRating() - o2.getRating();
            }
        });
        Trainee[] trainees = (Trainee[]) list.toArray();
        return new Group(group.getGroupName(), trainees);

    }


    public static Group sortingByFirstName(Group group) throws GroupException {
        List<Trainee> list = Arrays.asList(group.getTrainees());
        Collections.sort(list);
        Trainee[] trainees = (Trainee[]) list.toArray();
        return new Group(group.getGroupName(), trainees);
    }
/*
    public static trainee searchByName(group group, String firstName) throws TraineeException, GroupException {
        sortingByFirstName(group);
        List<trainee> list = Arrays.asList(group.getTrainees());
       trainee trainee= Collections.binarySearch(list, firstName);
        return trainee;
    }
*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (groupName != null ? !groupName.equals(group.groupName) : group.groupName != null) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(trainees, group.trainees);
    }

    @Override
    public int hashCode() {
        int result = groupName != null ? groupName.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(trainees);
        return result;
    }


}

