package ru.omsu.group;

public enum GroupErrorCodes {
    WRONG_GROUPNAME("Wrong group name %s"),
    WRONG_TRAINEES("Wrong trainees %s");


    private String message;

    private GroupErrorCodes(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

