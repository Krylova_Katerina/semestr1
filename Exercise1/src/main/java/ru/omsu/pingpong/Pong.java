package ru.omsu.pingpong;

public class Pong implements Runnable {
    private PingPong q;

    public Pong(PingPong q) {
        this.q = q;
        new Thread(this, "Pong").start();
    }

    public void run() {
        while (true)
            q.getPong();
    }
}
