package ru.omsu.pingpong;

import java.util.concurrent.Semaphore;

public class PingPong {
    private Semaphore semPing = new Semaphore(0);
    private Semaphore semPong = new Semaphore(1);

    public void getPong() {
        try {
            semPing.acquire();
        } catch (InterruptedException e) {
            System.out.println("InterruptedException");
        } finally {
            System.out.println("Pong");
            semPong.release();
        }
    }

    public void getPing() {
        try {
            semPong.acquire();
        } catch (InterruptedException e) {
            System.out.println("InterruptedException");
        } finally {
            System.out.println("Ping");
            semPing.release();
        }
    }
}
