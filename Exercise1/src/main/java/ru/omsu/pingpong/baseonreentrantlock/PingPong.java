package ru.omsu.pingpong.baseonreentrantlock;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

class PingPong implements Runnable {
    private Condition lockPing;
    private Condition lockPong;
    private ReentrantLock lock = new ReentrantLock();
    boolean flag;

    public PingPong(Condition lockPing, Condition lockPong, boolean flag) {
        this.lockPing = lockPing;
        this.lockPong = lockPong;
        this.flag = flag;
        new Thread(this).start();
    }

    @Override
    public void run() {
        while (true) {
            if (flag) {
                try {
                    lockPong.await();
                    System.out.println("Ping");
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                } finally {
                    lockPong.signal();
                }
            } else {
                try {
                    lockPing.await();
                    System.out.println("Pong");
                    Thread.sleep(100);
                } catch (InterruptedException e) {

                } finally {
                    lockPing.signal();
                }
            }
        }

    }

}