package ru.omsu.pingpong;

public class Ping implements Runnable {
    private PingPong q;

    public Ping(PingPong q) {
        this.q = q;
        new Thread(this, "Ping").start();
    }

    public void run() {
        while (true)
            q.getPing();
    }
}
