package ru.omsu.multistagetasks;


public class Stage implements Executable {
    private int number;


    public Stage(int number) {
        this.number=number;
    }

    @Override
    public void execute() {

        System.out.println("Stage "+getNumber()+" do something");
    }



    public int getNumber() {
        return number;
    }


}
