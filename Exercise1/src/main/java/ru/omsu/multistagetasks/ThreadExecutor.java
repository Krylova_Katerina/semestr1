package ru.omsu.multistagetasks;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;

public class ThreadExecutor extends Thread {
    private BlockingQueue<Task> queue;
   private CountDownLatch exit;

    public ThreadExecutor(BlockingQueue<Task> queue, CountDownLatch exit) {
        this.queue = queue;
        this.exit = exit;

    }

    @Override
    public void run() {
        System.out.println("ThreadExecutor Started");
        try {
            boolean flag = true;
            while (flag) {
                Task task = queue.take();
                if (!(task.isEnd())) {
                    if( exit.getCount()>0) {
                        System.out.println("ThreadExecutor read: " + task);
                        if (task.getCurrentStage() < task.getList().size()) {
                            task.getList().get(task.getCurrentStage()).execute();
                            task.setCurrentStage(task.getCurrentStage() + 1);
                            queue.put(task);
                        } else {
                            exit.countDown();
                        }
                    }
                } else{
                    task.getList().get(task.getCurrentStage()).execute();
                    flag = false;
                }
            }
        } catch (InterruptedException e) { }
        System.out.println("ThreadExecutor finished");
    }
}


