package ru.omsu.multistagetasks;

public interface Executable {
    void execute();
}

