package ru.omsu.multistagetasks;

import java.util.List;

public interface Task  {
    boolean isEnd();

    int getCurrentStage();

    List<Executable> getList();

    void setCurrentStage(int currentStage);
}
