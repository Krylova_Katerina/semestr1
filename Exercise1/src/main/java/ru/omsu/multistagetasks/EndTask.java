package ru.omsu.multistagetasks;

import java.util.List;

public class EndTask implements Task {
    @Override
    public boolean isEnd() {
        return true;
    }

    @Override
    public int getCurrentStage() {
        return 0;
    }

    @Override
    public List<Executable> getList() {
        return null;
    }

    @Override
    public void setCurrentStage(int currentStage) {

    }

}
