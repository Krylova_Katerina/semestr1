package ru.omsu.multistagetasks;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class ThreadDeveloper extends Thread {
    private BlockingQueue<Task> queue;
    private int count;
    final int SIZE=4;

    public ThreadDeveloper(BlockingQueue<Task> queue, int count) {
        this.queue = queue;
        this.count = count;
    }

    @Override
    public void run() {
        System.out.println("ThreadDeveloper Started");

        for (int i = 0; i < count; i++) {
            try {
                queue.put(new RegularTask(createListOfStage(), i));
                System.out.println("ThreadDeveloper added: RegularTask" + i);
            } catch (InterruptedException e) { }
        }
        System.out.println("ThreadDeveloper finished");
    }


    public List<Executable> createListOfStage() {
        List<Executable> list = new ArrayList<>();
        for (int i = 0; i < SIZE; i++) {
            list.add(new Stage(i));
        }
        return list;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ThreadDeveloper that = (ThreadDeveloper) o;

        if (count != that.count) return false;
        return queue != null ? queue.equals(that.queue) : that.queue == null;
    }

    @Override
    public int hashCode() {
        int result = queue != null ? queue.hashCode() : 0;
        result = 31 * result + count;
        return result;
    }

    @Override
    public String toString() {
        return "ThreadDeveloper{" +
                "queue=" + queue +
                ", count=" + count +
                '}';
    }
}



