package ru.omsu.multistagetasks;

import ru.omsu.threads.fortask.EndTask;

import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;


public class Demo {

    public static void main(String[] args) {

        System.out.println("Enter cont task");
        Scanner in = new Scanner(System.in);
        int count = in.nextInt();
        System.out.println("Enter cont Developer");
        int countDeveloper = in.nextInt();
        System.out.println("Enter cont Executor");
        int countExecutor = in.nextInt();

        BlockingQueue<Task> queue = new LinkedBlockingQueue<>();

        ThreadDeveloper[] developers = new ThreadDeveloper[countDeveloper];
        for (int i = 0; i < countDeveloper; i++) {
            developers[i] = new ThreadDeveloper(queue, count);
            developers[i].start();
        }

        ThreadExecutor[] executors = new ThreadExecutor[countExecutor];
        for (int i = 0; i < countExecutor; i++) {
            executors[i] =new ThreadExecutor(queue, new CountDownLatch(count));
            executors[i].start();
        }

        for (int i = 0; i < countDeveloper; i++) {
            try {
                developers[i].join();
            } catch (InterruptedException e) {
            }
        }

        for (int i = 0; i < countExecutor; i++) {
            try {
                executors[i].join();
            } catch (InterruptedException e) {
            }
        }

        for(int i=0; i<countExecutor; i++){
            queue.add((Task) new EndTask());
        }


    }
}

