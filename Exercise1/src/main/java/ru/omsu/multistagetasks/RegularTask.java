package ru.omsu.multistagetasks;

import java.util.List;

public class RegularTask implements Task {
private List<Executable> list;
private int name;
private int currentStage;

    public RegularTask() {

    }


    public List<Executable> getList() {
        return list;
    }


    public RegularTask(List<Executable> list, int name) {
        this.list = list;
        this.name=name;
        this.currentStage=0;
    }

    @Override
    public boolean isEnd() {
        return false;
    }


    public void setCurrentStage(int currentStage) {
        this.currentStage = currentStage;
    }

    public int getCurrentStage() {
        return currentStage;
    }

    @Override
    public String toString() {
        return "RegularTask{" +
                "list=" + list +
                ", name=" + name +
                ", currentStage=" + currentStage +
                '}';
    }
}
