package ru.omsu.java8;


import ru.omsu.trainee.Trainee;
import ru.omsu.trainee.TraineeException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Demo {

     public static Trainee create (String str){//5
         Trainee trainee = null;
         try {
             trainee = new Trainee(str, "lokiv", 3);
         } catch (TraineeException e) {
         }
         return trainee;
     }

    public static void main(String[] args) {
        Trainee trainee = create("Thor");//5
        Function<String, Trainee> myCreate = Demo::create;
        Trainee trainee1 = myCreate.apply("Thor");


        BiFunction<IntStream, IntUnaryOperator, IntStream> transform =  (stream, op) -> stream.map(op); //13
        IntStream intStream = IntStream.of(1, 2, 3);
        IntStream intStream1 = transform.apply(intStream, p->p+1);
        intStream1.forEach(System.out::println);

        BiFunction<IntStream, IntUnaryOperator, IntStream> parallelsTransform = (stream, op) -> stream.map(op).parallel(); //14
        IntStream intStream3 = IntStream.of(1, 2, 3);
        IntStream intStream2 = parallelsTransform.apply(intStream3, p->p+1);
      intStream1.forEach(System.out::println);


        Function<List<Trainee>, List<Trainee>> wantedTrainee =  list ->{ //15
            Stream<Trainee> stream= list.stream();
            Stream<Trainee> newList = stream.filter(t->t.getRating()>3);
            newList.sorted(); //В классе trainee реализован compareTo по имени
            return Arrays.asList((Trainee)newList);
        };

        Function<List<Trainee>, List<Trainee>> wantedTrainee2 =  list ->{ //16
            Stream<Trainee> stream= list.stream();
            Stream<Trainee> newList = stream.filter(t->t.getRating()>3).sorted();
            newList.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            return Arrays.asList((Trainee)newList);
        };

        List<Trainee> trainees = new ArrayList<>();
        try {
            trainees.add(new Trainee("Loki", "Loki", 5));
            trainees.add(new Trainee("Thor", "Loki", 4));
            trainees.add(new Trainee("Loki", "Lokiv", 5));
            trainees.add(new Trainee("Tir", "Loki", 3));

        } catch (TraineeException e) { }

        List<Trainee> trainees1 = wantedTrainee2.apply(trainees);
        for(Trainee t: trainees1){
            System.out.println(t.toString());
        }

        Function<List<Integer>, Integer> sum= list ->{ //17
           Stream<Integer> listStream = list.stream();
           Optional<Integer> result = listStream.reduce(Integer::sum);
           return result.get();
       };
       List<Integer> integers = new ArrayList<>();
       integers.add(4);
       integers.add(5);
       integers.add(6);
       System.out.println(sum.apply(integers));


        Function<List<Integer>, Integer> product= list ->{ //17
            Stream<Integer> listStream = list.stream();
            Optional<Integer> result = listStream.reduce( (a,b) -> a*b);
            return result.get();
        };

        System.out.println(product.apply(integers));
    }



}
