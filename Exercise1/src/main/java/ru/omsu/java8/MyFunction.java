package ru.omsu.java8;
@FunctionalInterface
public interface MyFunction {

   public Object apply(Object arg);
}
