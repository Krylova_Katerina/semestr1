package ru.omsu;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Matrix {
    private String[] matrix;

    public static Set<Set<String>> searchSimilarStrings(String[] matrix) {
        Set<Set<String>> newMatrix = new HashSet<>();
        for (int i = 0; i < matrix.length; i++) {
            Set<String> nextLine = new HashSet<>(Arrays.asList(matrix[i].split(" ")));
            newMatrix.add(nextLine);
        }
        return newMatrix;
    }
}
