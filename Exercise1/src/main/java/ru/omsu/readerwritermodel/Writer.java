package ru.omsu.readerwritermodel;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Writer implements Runnable {
    static private int i = 0;
    static private ReadWriteLock lock = new ReentrantReadWriteLock();


    @Override
    public void run() {
        lock.writeLock().lock();
        System.out.println("  Write lock locked by thread " + Thread.currentThread().getId());
        try {
            i++;
            System.out.println("  Writer thread " + Thread.currentThread().getId() + " set value " + i);
        } finally {
            System.out.println("  Write lock unlocked by thread " + Thread.currentThread().getId());
            lock.writeLock().unlock();
        }
    }
}

