package ru.omsu.readerwritermodel;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Reader implements Runnable {
    static private int i = 0;
    static private ReadWriteLock lock = new ReentrantReadWriteLock();

    @Override
    public void run() {
        lock.readLock().lock();
        System.out.println("Read lock locked by thread " + Thread.currentThread().getId());
        try {
            System.out.println("Reader thread " + Thread.currentThread().getId() + " read value " + i);
        } finally {
            System.out.println("Read lock unlocked by thread " + Thread.currentThread().getId());
            lock.readLock().unlock();
        }
    }
}
