package ru.omsu.threads.dataqueue;

import java.util.Arrays;

public class RegularData implements Data {
    private int[] data;

    public RegularData(int[] data) {
        this.data = data;
    }

    public int[] getDat() {
        return data;
    }

    @Override
    public boolean isEnd() {
        return false;
    }

    @Override
    public String toString() {
        return "RegularData{" +
                "data=" + Arrays.toString(data) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegularData that = (RegularData) o;

        return Arrays.equals(data, that.data);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(data);
    }
}
