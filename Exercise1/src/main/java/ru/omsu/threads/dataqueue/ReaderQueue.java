package ru.omsu.threads.dataqueue;

import java.util.concurrent.BlockingQueue;

public class ReaderQueue extends Thread {
    private BlockingQueue<Data> queue;


    public ReaderQueue(BlockingQueue<Data> queue) {
        this.queue = queue;

    }


    @Override
    public void run() {
        System.out.println("Reader Started");
        try {
            boolean flag = true;
            while (flag) {
                Data date = queue.take();
                if (!(date.isEnd())) {
                    System.out.println("Reader read: " + date);
                } else {
                    System.out.println("Reader : empty queue");
                    flag=false;
                }
            }
        } catch (InterruptedException e) { }
            System.out.println("Reader finished");
    }
}