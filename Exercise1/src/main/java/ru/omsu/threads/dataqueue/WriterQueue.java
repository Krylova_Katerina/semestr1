package ru.omsu.threads.dataqueue;

import java.util.concurrent.BlockingQueue;

public class WriterQueue extends Thread {
    private BlockingQueue<Data> queue;
    private int count;

    public WriterQueue(BlockingQueue<Data> queue, int count) {
        this.queue = queue;
        this.count = count;
    }

    @Override
    public void run() {
        System.out.println("Writer Started");
        for (int i = 0; i < count; i++) {
            try {
                queue.put(new RegularData(createArrayForData(count)));
                System.out.println("Writer added: elem" + i);
            } catch (InterruptedException e) { }
        }
      /*  for (int j=0; j<count; j++){
            try {
                queue.put(new EndData());
            } catch (InterruptedException e) { }
        }*/
        System.out.println("Writer finished");
    }

    public int[] createArrayForData(int count){
        int[] result = new int[count];
        for (int i=0; i<count; i++){
            result[i]= count+i;
        }
        return result;


    }

}



