package ru.omsu.threads.forformatter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Formatter {

    private static ThreadLocal<SimpleDateFormat> lock = new ThreadLocal<>();

    public static void format(Date date) {
        lock.set(new SimpleDateFormat());
        System.out.println(lock.get().format(date));
    }

}
