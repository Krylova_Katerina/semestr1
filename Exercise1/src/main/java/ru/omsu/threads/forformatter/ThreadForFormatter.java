package ru.omsu.threads.forformatter;

import java.util.Date;

import static ru.omsu.threads.forformatter.Formatter.format;

public class ThreadForFormatter implements Runnable {
    private Date date;

    public ThreadForFormatter(Date date) {
        this.date=date;
        new Thread(this).start();
    }

    @Override
    public void run() {
       format(date);
    }


}
