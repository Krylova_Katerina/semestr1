package ru.omsu.threads.forsendmessege;

import ru.omsu.fileoperations.FileOperationsException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Transport implements Runnable {
    private Message message;

    public Transport(Message message) {
        this.message = message;
        new Thread(this).start();
    }

    public static void send(Message message) throws FileOperationsException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(message.getEmailAddress()))) {
            bw.write(message.getSender());
            bw.newLine();
            bw.write(message.getEmailAddress());
            bw.newLine();
            bw.write(message.getSubject());
            bw.newLine();
            bw.write(message.getBody());
            bw.newLine();
        } catch (IOException e) {
            throw new FileOperationsException(e);
        }

    }

    @Override
    public void run() {
        try {
            send(message);
        } catch (FileOperationsException e) {
        }
    }

}
