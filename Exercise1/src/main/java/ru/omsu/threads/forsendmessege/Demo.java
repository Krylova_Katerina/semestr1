package ru.omsu.threads.forsendmessege;

import ru.omsu.fileoperations.FileOperationsException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Demo {

    public static void main(String args[]) throws FileOperationsException {
            List<String> emails = new ArrayList<>();
            try (BufferedReader br = new BufferedReader(new FileReader("email.txt"))) {
                for (int i=0; i<5; i++){
                    emails.add(br.readLine());}
            } catch (IOException e) {
                throw new FileOperationsException(e);
            }

        ExecutorService executor = Executors.newFixedThreadPool(5);
        for (int i = 0; i < emails.size(); i++) {
            Runnable worker = new Transport(new Message(
                    emails.get(i),
                    "Groot",
                    "I'm Groot",
                    "Hello, I'm Groot!!!"));
            executor.execute(worker);
        }
        executor.shutdown();
    }
}
