package ru.omsu.threads.threadsforarraylist;

import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadForALOnBaseReentrantLock implements Runnable {
    private ReentrantLock lock;
    private String name;
    private List<Integer> list;
    private boolean flag;

    public ThreadForALOnBaseReentrantLock(ReentrantLock lk, String nameSt, List<Integer> lisrAL, boolean flagLock) {
        name = nameSt;
        list = lisrAL;
        lock = lk;
        flag = flagLock;
        new Thread(this, name).start();
    }

    @Override
    public void run() {
        try {
            if (flag) {
                final Random random = new Random();
                for (int i = 0; i < 100000; i++) {
                    lock.lock();
                    list.add(random.nextInt(500));
                    System.out.println("ADD: " + list.get(i));
                }
            } else {
                for (int i = 0; i < 100000; i++) {
                    lock.lock();
                    if (list.size() != 0) {
                        list.remove(i);
                        System.out.println("delete: " + i);
                    }
                }
            }
        } finally {
            lock.unlock();
        }

    }
}
