package ru.omsu.threads.threadsforarraylist;

import java.util.List;
import java.util.Random;

public class ThreadThreeForArrayList implements Runnable {
    private String name;
    private Thread t;
    private List<Integer> list;
    private boolean flag;

    public boolean isFlag() {
        return flag;
    }


    public synchronized void addElem(int i) {
        final Random random = new Random();
        list.add(i, random.nextInt(500));
        System.out.println("ADD: " + list.get(i));

    }

    public synchronized void deleteElem(int i) {
        if (list.size() != 0) {
            list.remove(i);
            System.out.println("delete: " + i);
        }

    }

    @Override
    public void run() {
        if (isFlag()) {
            for (int i = 0; i < 100000; i++) {
                addElem(i);
            }
        } else if (!isFlag()) {
            for (int i = 0; i < 100000; i++) {
                deleteElem(i);
            }
        }
        System.out.println(name + " exiting.");

    }

    public ThreadThreeForArrayList(String threadName, List<Integer> listInt, boolean flagInt) {
        name = threadName;
        list = listInt;
        flag = flagInt;
        t = new Thread(this, name);
        t.start();
    }

}


