package ru.omsu.threads.threadsforarraylist;

import java.util.List;

public class ThreadTwoForArrayList implements Runnable {
    private String name;
    private Thread t;
    private List<Integer> list;

    public List<Integer> getList() {
        return list;
    }

    public synchronized void deleteElem(int i) {
        if (list.size() != 0) {
            list.remove(i);
            System.out.println("delete: " + i);
        }

    }

    @Override
    public void run() {
        for (int i = 0; i < 100000; i++) {
            deleteElem(i);
        }
        System.out.println(name + " exiting.");

    }

    public ThreadTwoForArrayList(String threadName, List<Integer> listInt) {
        name = threadName;
        list = listInt;
        t = new Thread(this, name);
        t.start();
    }


}
