package ru.omsu.threads.threadsforarraylist;

import java.util.List;
import java.util.Random;

public class ThreadOneForArrayList implements Runnable {
    private String name;
    private Thread t;
    private List<Integer> list;

    public synchronized void addElem(int i) {
        final Random random = new Random();
        list.add(i, random.nextInt(500));
        System.out.println("ADD: " + list.get(i));

    }


    @Override
    public void run() {
        for (int i = 0; i < 100000; i++) {
            addElem(i);
        }
        System.out.println(name + " exiting.");

    }

    public ThreadOneForArrayList(String threadName, List<Integer> listInt) {
        name = threadName;
        list = listInt;
        t = new Thread(this, name);
        t.start();
    }

}
