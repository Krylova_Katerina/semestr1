package ru.omsu.threads.fortask;

import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Demo {
    public static void main(String[] args) {
        System.out.println("Enter cont task");
        Scanner in = new Scanner(System.in);
        int count =  in.nextInt();
        System.out.println("Enter cont Developer");
        int countDeveloper =  in.nextInt();
        System.out.println("Enter cont Executor");
        int countExecutor =  in.nextInt();

        BlockingQueue<Executable> queue = new LinkedBlockingQueue<>();

        ThreadDeveloper[] threadDevelopers = new ThreadDeveloper[countDeveloper];
        for(int i=0; i<countDeveloper; i++){
            threadDevelopers[i] =new ThreadDeveloper(queue, count);
            threadDevelopers[i].start();
        }

        ThreadExecutor[] threadExecutors = new ThreadExecutor[countExecutor];
        for(int i=0; i<countExecutor; i++){
            threadExecutors[i] = new ThreadExecutor(queue);
            threadExecutors[i].start();
        }

        for(int i=0; i<countDeveloper; i++){
            try {
                threadDevelopers[i].join();
            } catch (InterruptedException e) { }
        }

        for(int i=0; i<countExecutor; i++){
            try {
                threadExecutors[i].join();
            } catch (InterruptedException e) { }
        }

        for(int i=0; i<countExecutor; i++){
            queue.add(new EndTask());
        }

    }
}
