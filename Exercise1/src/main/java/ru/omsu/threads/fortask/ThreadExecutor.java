package ru.omsu.threads.fortask;

import java.util.concurrent.BlockingQueue;

public class ThreadExecutor extends Thread {
    private BlockingQueue<Executable> queue;

    public ThreadExecutor(BlockingQueue<Executable> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        System.out.println("ThreadExecutor Started");
        try {
            boolean flag= true;
            while (flag){
                Executable task = queue.take();
                if (!(task.isEnd())) {
                    System.out.println("ThreadExecutor read: " + task);
                    task.execute();
                } else {
                    System.out.println("ThreadExecutor : empty queue");
                    flag=false;
                }
            }
        } catch (InterruptedException e) { }
        System.out.println("ThreadExecutor finished");
    }
}
