package ru.omsu.threads.fortask;

import java.util.concurrent.BlockingQueue;

public class ThreadDeveloper extends Thread {
    private BlockingQueue<Executable> queue;
    private int count;

    public ThreadDeveloper(BlockingQueue<Executable> queue, int count) {
        this.queue = queue;
        this.count = count;
    }

    @Override
    public void run() {
        System.out.println("ThreadDeveloper Started");
        for (int i = 0; i < count; i++) {
            try {
                queue.put(new Task(i));
                System.out.println("ThreadDeveloper added: elem" + i);
            } catch (InterruptedException e) { }
        }
     /*   for (int i =0; i<count; i++){
            try {
                queue.put(new EndTask());
            } catch (InterruptedException e) { }
        }*/
        System.out.println("ThreadDeveloper finished");
    }
}



