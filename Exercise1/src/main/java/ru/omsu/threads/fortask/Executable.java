package ru.omsu.threads.fortask;

public interface Executable {
    void execute();

    boolean isEnd();

}

