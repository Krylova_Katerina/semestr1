package ru.omsu.threads.fortask;

public class Task implements Executable {

private int name;


    public Task(int name) {
        this.name=name;

    }


    public int getName() {
        return name;
    }

    @Override
    public void execute() {
        System.out.println("Hello, I'm Groot!");
    }

    @Override
    public boolean isEnd() {
        return false;
    }

    @Override
    public String toString() {
        return "RegularTask{" +
                "name=" + name +
                '}';
    }
}
