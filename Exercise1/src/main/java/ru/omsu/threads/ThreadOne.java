package ru.omsu.threads;

public class ThreadOne implements Runnable {
    private String name;
    private Thread t;

    @Override
    public void run() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            System.out.println(name + " thread interrupted.");
        }
        System.out.println(name + " exiting.");
    }

    public ThreadOne(String threadName) {
        name = threadName;
        t = new Thread(this, name);
        t.start();
    }

    public Thread getT() {
        return t;
    }
}
