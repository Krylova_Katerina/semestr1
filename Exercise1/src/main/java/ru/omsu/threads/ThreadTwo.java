package ru.omsu.threads;

public class ThreadTwo implements Runnable {
    private String name;
    private Thread t;

    @Override
    public void run() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            System.out.println(name + " thread interrupted.");
        }
        System.out.println(name + " exiting.");
    }

    public ThreadTwo(String threadName) {
        name = threadName;
        t = new Thread(this, name);
        System.out.println("New thread: " + t);
        t.start();
    }

    public Thread getT() {
        return t;
    }
}
