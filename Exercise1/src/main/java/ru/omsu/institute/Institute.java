package ru.omsu.institute;

import ru.omsu.trainee.Trainee;

import java.util.Map;

public class Institute {
    private String instituteName;
    private String instituteCity;

    public Institute(String instituteName, String instituteCity) throws InstituteException {
        setInstituteName(instituteName);
        setInstituteCity(instituteCity);
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) throws InstituteException {
        if (instituteName == null || instituteName.length() == 0)
            throw new InstituteException(InstituteErrorCodes.WRONG_INSTITUTENAME, instituteName);
        this.instituteName = instituteName;
    }

    public String getInstituteCity() {
        return instituteCity;
    }

    public void setInstituteCity(String instituteCity) throws InstituteException {
        if (instituteCity == null || instituteCity.length() == 0)
            throw new InstituteException(InstituteErrorCodes.WRONG_INSTITUTECITY, instituteCity);
        this.instituteCity = instituteCity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Institute institute = (Institute) o;

        if (instituteName != null ? !instituteName.equals(institute.instituteName) : institute.instituteName != null)
            return false;
        return instituteCity != null ? instituteCity.equals(institute.instituteCity) : institute.instituteCity == null;
    }

    @Override
    public int hashCode() {
        int result = instituteName != null ? instituteName.hashCode() : 0;
        result = 31 * result + (instituteCity != null ? instituteCity.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "institute{" +
                "instituteName='" + instituteName + '\'' +
                ", instituteCity='" + instituteCity + '\'' +
                '}';
    }

    public static void printAllTraineeFromInstituteMap(Map<Trainee, Institute> traineeInstituteMap) {
        for (Map.Entry entry : traineeInstituteMap.entrySet()) {
            System.out.println(entry.getKey().toString());
        }
    }

    public static void printTraineeFromInstituteMap(Map<Trainee, Institute> traineeInstituteMap) {
        for (Map.Entry entry : traineeInstituteMap.entrySet()) {
            System.out.println(entry.getKey().toString() + "  " + entry.getValue().toString());
        }
    }
}
