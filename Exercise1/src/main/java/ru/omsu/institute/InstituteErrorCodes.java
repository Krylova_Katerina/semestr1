package ru.omsu.institute;

public enum InstituteErrorCodes {
    WRONG_INSTITUTENAME("Wrong institute name %s"),
    WRONG_INSTITUTECITY("Wrong institute city %s");

    private String message;

    private InstituteErrorCodes(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

