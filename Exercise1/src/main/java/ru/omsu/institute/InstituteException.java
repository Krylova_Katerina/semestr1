package ru.omsu.institute;

public class InstituteException extends Throwable {
    public InstituteException(InstituteErrorCodes errorCode, String cause) {
        super(String.format(errorCode.getMessage(), cause));
    }
}
