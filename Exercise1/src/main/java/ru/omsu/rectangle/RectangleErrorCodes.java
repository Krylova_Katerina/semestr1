package ru.omsu.rectangle;

/**
 * Created by User on 06.09.2017.
 */
enum RectangleErrorCodes {
    WRONG_PARAMETERS("Wrong parameter/parameters");
    private String message;

    private RectangleErrorCodes(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}