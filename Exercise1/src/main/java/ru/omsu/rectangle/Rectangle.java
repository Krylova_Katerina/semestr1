package ru.omsu.rectangle;

import java.io.Serializable;

/**
 * Created by User on 06.09.2017.
 */
public class Rectangle implements Serializable {
    private double left;
    private double top;
    private double right;
    private double bottom;

    public Rectangle() {
    }

    public Rectangle(double left, double top, double right, double bottom) {
        this.left = left;
        this.right = right;
        this.top = top;
        this.bottom = bottom;
    }

    public double getLeft() {
        return left;
    }

    public void setLeft(double left) {
        this.left = left;
    }

    public double getRight() {
        return right;
    }

    public void setRight(double right) {
        this.right = right;
    }

    public double getTop() {
        return top;
    }

    public void setTop(double top) {
        this.top = top;
    }

    public double getBottom() {
        return bottom;
    }

    public void setBottom(double bottom) {
        this.bottom = bottom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rectangle rectangle = (Rectangle) o;

        if (Double.compare(rectangle.left, left) != 0) return false;
        if (Double.compare(rectangle.top, top) != 0) return false;
        if (Double.compare(rectangle.right, right) != 0) return false;
        return Double.compare(rectangle.bottom, bottom) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(left);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(top);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(right);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(bottom);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
