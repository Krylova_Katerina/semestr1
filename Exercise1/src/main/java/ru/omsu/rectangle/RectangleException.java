package ru.omsu.rectangle;

/**
 * Created by User on 06.09.2017.
 */
public class RectangleException extends Exception {


    public RectangleException(RectangleErrorCodes errorCode) {
        super(errorCode.getMessage());
    }
}

