package ru.omsu.person;

public enum PersonErrorCodes {

    WRONG_PERSON_MOTHER("person's mother = null"),
    WRONG_PERSON_FATHER("person's father = null");


    private String message;

    private PersonErrorCodes(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
