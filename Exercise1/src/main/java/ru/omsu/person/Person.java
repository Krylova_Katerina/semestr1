package ru.omsu.person;

public class Person {
    private Person personMother;
    private Person personFather;

    public Person(Person personMother, Person personFather) {
        setPersonMother(personMother);
        setPersonFather(personFather);
    }

    public void setPersonMother(Person personMother) {
        this.personMother = personMother;
    }

    public void setPersonFather(Person personFather) {
        this.personFather = personFather;
    }

    public Person getPersonMother() {
        return personMother;
    }

    public Person getPersonFather() {
        return personFather;
    }

    public Person getMothersMotherFather(Person person) {
        if (person.getPersonFather() == null) {
            return null;
        } else if (person.getPersonFather().getPersonMother() == null) {
            return null;
        } else if (person.getPersonFather().getPersonMother().getPersonMother() == null) {
            return null;
        }
        return person.getPersonFather().getPersonMother().getPersonMother();
    }


}
