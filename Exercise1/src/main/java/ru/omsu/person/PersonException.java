package ru.omsu.person;

public class PersonException extends Throwable {


    public PersonException(PersonErrorCodes errorCode) {
        super(String.format(errorCode.getMessage()));
    }
}
