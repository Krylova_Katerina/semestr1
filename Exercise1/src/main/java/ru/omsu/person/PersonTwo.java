package ru.omsu.person;

import java.util.Optional;

public class PersonTwo {
    private Optional<Person> mother;
    private Optional<Person> father;


    public PersonTwo(Optional<Person> mother, Optional<Person> father) {
        this.mother = mother;
        this.father = father;
    }

    public Optional<Person> getMother() {
        return mother;
    }

    public Optional<Person> getFather() {
        return father;
    }

   public Optional<Person> getMothersMotherFather (){
        return father.flatMap(person -> {
        return  Optional.of(person.getPersonMother());
        }).flatMap( person -> {
            return Optional.of(person.getPersonMother());
        });
    }
}
